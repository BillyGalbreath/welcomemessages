package net.pl3x.bukkit.welcomemessages.command;

import net.pl3x.bukkit.welcomemessages.Chat;
import net.pl3x.bukkit.welcomemessages.WelcomeMessages;
import net.pl3x.bukkit.welcomemessages.configuration.Config;
import net.pl3x.bukkit.welcomemessages.configuration.Lang;
import net.pl3x.bukkit.welcomemessages.configuration.PlayerConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Collections;
import java.util.List;

public class CmdWelcomeMessages implements TabExecutor {
    private final WelcomeMessages plugin;

    public CmdWelcomeMessages(WelcomeMessages plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            return Collections.singletonList("reload");
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.welcomemessages")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            Config.reload();
            PlayerConfig.removeConfigs();

            new Chat(Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()))
                    .send(sender);
            return true;
        }

        new Chat(Lang.VERSION
                .replace("{version}", plugin.getDescription().getVersion())
                .replace("{plugin}", plugin.getName()))
                .send(sender);
        return true;
    }
}
