package net.pl3x.bukkit.welcomemessages.command;

import net.pl3x.bukkit.welcomemessages.Chat;
import net.pl3x.bukkit.welcomemessages.WelcomeMessages;
import net.pl3x.bukkit.welcomemessages.configuration.Lang;
import net.pl3x.bukkit.welcomemessages.configuration.PlayerConfig;
import net.pl3x.bukkit.welcomemessages.task.TipTask;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CmdWelcomeTips implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> result = new ArrayList<>();
        if (args.length == 1) {
            String arg = args[0].toLowerCase();
            if ("enable".startsWith(arg)) {
                result.add("enable");
            }
            if ("disable".startsWith(arg)) {
                result.add("disable");
            }
            if ("on".startsWith(arg)) {
                result.add("on");
            }
            if ("off".startsWith(arg)) {
                result.add("off");
            }
        }
        return result;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.welcometips")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }

        PlayerConfig pConfig = PlayerConfig.getConfig((Player) sender);

        if (args.length == 0) {
            new TipTask((Player) sender).runTask(WelcomeMessages.getPlugin());
            return true;
        }

        String arg = args[0].toLowerCase();
        boolean enable = false;
        if ("enable".startsWith(arg)) {
            enable = true;
        } else if ("disable".startsWith(arg)) {
            enable = false;
        } else if ("on".startsWith(arg)) {
            enable = true;
        } else if ("off".startsWith(arg)) {
            enable = false;
        } else if ("1".startsWith(arg)) {
            enable = false;
        } else if ("0".startsWith(arg)) {
            enable = false;
        }

        pConfig.setTipsEnabled(enable);

        new Chat(Lang.TIPS_TOGGLED
                .replace("{action}", BooleanUtils.toStringOnOff(enable)))
                .send(sender);
        return true;
    }
}
