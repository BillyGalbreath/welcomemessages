package net.pl3x.bukkit.welcomemessages;

import net.pl3x.bukkit.welcomemessages.command.CmdWelcomeMessages;
import net.pl3x.bukkit.welcomemessages.command.CmdWelcomeTips;
import net.pl3x.bukkit.welcomemessages.configuration.Config;
import net.pl3x.bukkit.welcomemessages.configuration.Lang;
import net.pl3x.bukkit.welcomemessages.configuration.PlayerConfig;
import net.pl3x.bukkit.welcomemessages.listener.PlayerListener;
import net.pl3x.bukkit.welcomemessages.task.TipTask;
import org.bukkit.plugin.java.JavaPlugin;

public class WelcomeMessages extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        getServer().getPluginManager().registerEvents(new PlayerListener(this), this);

        getCommand("welcomemessages").setExecutor(new CmdWelcomeMessages(this));
        getCommand("welcometips").setExecutor(new CmdWelcomeTips());

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getServer().getScheduler().cancelTasks(this);
        TipTask.tipTasks.clear();

        PlayerConfig.removeConfigs();

        Logger.info(getName() + " disabled.");
    }

    public static WelcomeMessages getPlugin() {
        return WelcomeMessages.getPlugin(WelcomeMessages.class);
    }
}
