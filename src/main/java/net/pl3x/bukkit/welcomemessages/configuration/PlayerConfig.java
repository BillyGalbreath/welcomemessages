package net.pl3x.bukkit.welcomemessages.configuration;

import net.pl3x.bukkit.welcomemessages.WelcomeMessages;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerConfig extends YamlConfiguration {
    private static final Map<UUID, PlayerConfig> configs = new HashMap<>();

    public static PlayerConfig getConfig(Player player) {
        UUID uuid = player.getUniqueId();
        synchronized (configs) {
            if (configs.containsKey(uuid)) {
                return configs.get(uuid);
            }
            PlayerConfig config = new PlayerConfig(uuid);
            configs.put(uuid, config);
            return config;
        }
    }

    public static void removeConfigs() {
        Collection<PlayerConfig> oldConfs = new ArrayList<>(configs.values());
        synchronized (configs) {
            oldConfs.forEach(PlayerConfig::discard);
        }
    }

    private File file = null;
    private final Object saveLock = new Object();
    private final UUID uuid;

    private PlayerConfig(UUID uuid) {
        super();
        file = new File(WelcomeMessages.getPlugin().getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml");
        this.uuid = uuid;
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void discard() {
        synchronized (configs) {
            configs.remove(uuid);
        }
    }

    public boolean isTipsEnabled() {
        return getBoolean("tips-enabled", true);
    }

    public void setTipsEnabled(boolean enabled) {
        set("tips-enabled", enabled);
        save();
    }

    public int getTipCount() {
        return getInt("tip-count", 0);
    }

    public void setTipCount(int count) {
        set("tip-count", count);
        save();
    }
}
