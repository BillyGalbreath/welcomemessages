package net.pl3x.bukkit.welcomemessages.configuration;

import net.pl3x.bukkit.welcomemessages.WelcomeMessages;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for that command!";
    public static String PLAYER_COMMAND = "&4This command is only available to players.";
    public static String NOTIFY_NEW_PLAYER_JOINED = "&dPlease welcome {player} to the server!";
    public static String FIRST_TIME_JOIN_MESSAGE = "&dWelcome, {player}!\n&dEnjoy your stay! &e^_^";
    public static String JOIN_MESSAGE = "&dWelcome back, {player}!";
    public static String TIPS_PREFIX = "&e[&3Tip&e]&7: &d";
    public static String TIPS_TOGGLED = "&dTips toggled {action}";
    public static String VERSION = "&d{plugin} v{version}.";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload() {
        WelcomeMessages plugin = WelcomeMessages.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command!");
        PLAYER_COMMAND = config.getString("player-command", "&4This command is only available to players.");
        NOTIFY_NEW_PLAYER_JOINED = config.getString("notify-new-player-joined", "&dPlease welcome {player} to the server!");
        FIRST_TIME_JOIN_MESSAGE = config.getString("first-time-join-message", "&dWelcome, {player}!\n&dEnjoy your stay! &e^_^");
        JOIN_MESSAGE = config.getString("join-message", "&dWelcome back, {player}!");
        TIPS_PREFIX = config.getString("tips-prefix", "&e[&3Tip&e]&7: &d");
        TIPS_TOGGLED = config.getString("tips-toggled", "&dTips toggled {action}");
        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }
}
