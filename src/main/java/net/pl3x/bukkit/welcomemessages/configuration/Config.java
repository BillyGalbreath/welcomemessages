package net.pl3x.bukkit.welcomemessages.configuration;

import net.pl3x.bukkit.welcomemessages.WelcomeMessages;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static int TIPS_DELAY = 30;
    public static int TIPS_PERIOD = 600;
    public static List<String> SERVER_TIPS = new ArrayList<>();

    public static void reload() {
        WelcomeMessages plugin = WelcomeMessages.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        TIPS_DELAY = config.getInt("tips-delay", 30);
        TIPS_PERIOD = config.getInt("tips-period", 600);
        SERVER_TIPS = config.getStringList("server-tips");
    }
}
