package net.pl3x.bukkit.welcomemessages.task;

import net.pl3x.bukkit.welcomemessages.Chat;
import net.pl3x.bukkit.welcomemessages.Logger;
import net.pl3x.bukkit.welcomemessages.configuration.Config;
import net.pl3x.bukkit.welcomemessages.configuration.Lang;
import net.pl3x.bukkit.welcomemessages.configuration.PlayerConfig;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TipTask extends BukkitRunnable {
    public static final Map<UUID, TipTask> tipTasks = new HashMap<>();
    private final Player player;

    public TipTask(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        // double check if player disconnected
        if (!player.isOnline()) {
            tipTasks.remove(player.getUniqueId());
            cancel();
            return;
        }

        PlayerConfig pConfig = PlayerConfig.getConfig(player);

        //check if player has tips turned off
        if (!pConfig.isTipsEnabled()) {
            return;
        }

        // check tip count
        int tipsCount = pConfig.getTipCount();
        if (Config.SERVER_TIPS.size() <= tipsCount) {
            tipsCount = 0; // restart tips
        }

        // get and check tip
        String tip = Config.SERVER_TIPS.get(tipsCount);
        if (tip == null || tip.isEmpty()) {
            return;
        }

        // send tip
        Logger.debug("Sending tip #" + tipsCount + " to " + player.getName());
        new Chat(Lang.TIPS_PREFIX + tip).send(player);

        // increment
        pConfig.setTipCount(tipsCount + 1);
    }
}
