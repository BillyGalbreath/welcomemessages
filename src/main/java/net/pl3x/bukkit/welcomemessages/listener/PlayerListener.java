package net.pl3x.bukkit.welcomemessages.listener;

import net.pl3x.bukkit.welcomemessages.Chat;
import net.pl3x.bukkit.welcomemessages.Logger;
import net.pl3x.bukkit.welcomemessages.WelcomeMessages;
import net.pl3x.bukkit.welcomemessages.configuration.Config;
import net.pl3x.bukkit.welcomemessages.configuration.Lang;
import net.pl3x.bukkit.welcomemessages.task.TipTask;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {
    private final WelcomeMessages plugin;

    public PlayerListener(WelcomeMessages plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.isOnline()) {
            Logger.debug(player.getName() + " has joined the server, but was disconnected.");
            return;
        }
        Logger.debug(player.getName() + " has joined the server. Played before: " + player.hasPlayedBefore());

        // welcome the player
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            if (!player.isOnline()) {
                return; // check if player disconnected
            }
            if (player.hasPlayedBefore()) {
                new Chat(Lang.JOIN_MESSAGE
                        .replace("{player}", player.getName()))
                        .send(player);
            } else {
                new Chat(Lang.FIRST_TIME_JOIN_MESSAGE
                        .replace("{player}", player.getName()))
                        .send(player);
                for (Player online : Bukkit.getOnlinePlayers()) {
                    if (online.equals(player)) {
                        continue;
                    }
                    new Chat(Lang.NOTIFY_NEW_PLAYER_JOINED
                            .replace("{player}", player.getName()))
                            .send(online);
                }
            }
        }, 20);

        // send the player tips
        Logger.debug("Tips scheduled for " + player.getName());

        TipTask tipTask = new TipTask(player);
        int tipsPeriod = Config.TIPS_PERIOD;
        if (tipsPeriod <= 0) {
            tipTask.runTaskLater(plugin, Config.TIPS_DELAY * 20);
        } else {
            tipTask.runTaskTimer(plugin, Config.TIPS_DELAY * 20, tipsPeriod * 20);
        }

        TipTask previousTask = TipTask.tipTasks.put(player.getUniqueId(), tipTask);

        if (previousTask != null) {
            previousTask.cancel();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        TipTask task = TipTask.tipTasks.remove(event.getPlayer().getUniqueId());
        if (task != null) {
            task.cancel();
        }
    }
}
